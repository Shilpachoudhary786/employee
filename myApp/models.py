from django.db import models
from django.contrib.auth.models import User
class employee(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    country = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    contact = models.IntegerField(blank=True,null=True)
    image = models.ImageField(upload_to='images/%Y/%m/%d',blank=True,null=True)

    def __str__(self):
        return self.user.username

class HR(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    sub = (('Mr.','Mr.'),('Mrs.','Mrs.'),('Miss','Miss'))
    gender = models.CharField(choices=sub,max_length=10,blank=True,null=True)
    country = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    contact = models.IntegerField(blank=True,null=True)
    image = models.ImageField(upload_to='images/%Y/%m/%d',blank=True,null=True)

    def __str__(self):
        return self.user.username
 

class emp_details(models.Model):
    emp_name = models.ForeignKey(employee,on_delete=models.CASCADE)
    name = models.CharField(max_length=200,blank= True,null=True)
    sub = (('Mr.','Mr.'),('Mrs.','Mrs.'),('Miss','Miss'))
    title = models.CharField(choices=sub,max_length=10,blank=True,null=True)
    date_of_birth = models.DateField(blank= True,null=True)
    email = models.EmailField(max_length=200,blank= True)
    p_phone = models.IntegerField(blank= True,null=True)
    a_phone = models.IntegerField(blank= True,null=True)
    address = models.CharField(max_length=200,blank= True,null=True)
    city = models.CharField(max_length=100,blank= True,null=True)
    state = models.CharField(max_length=200,blank= True,null=True)
    country = models.CharField(max_length=200,blank= True,null=True)
    pin_code = models.IntegerField(blank= True,null=True)
    height = models.DecimalField(max_digits=10,decimal_places=2,blank= True,null=True)
    sub_group = (('A-','A-'),('A+','A+'),('B+','B+'),('B-','B-'),('O+','O+'),('O-','O-'),('AB+','AB+'),('AB-','AB-'))
    blood_group = models.CharField(choices=sub_group,max_length=10,blank= True,null=True)
    weight = models.DecimalField(max_digits=10,decimal_places=2,blank= True,null=True)
    disease = models.CharField(max_length=200,blank= True,null=True)
    sub_eye = (('Brown','Brown'),('Black','Black'))
    left_eye = models.CharField(choices=sub_eye,max_length=20,blank= True,null=True)
    right_eye = models.CharField(choices=sub_eye,max_length=20,blank= True,null=True)
    left_ear = models.CharField(max_length=200,blank= True,null=True)
    right_ear = models.CharField(max_length=200,blank= True,null=True)
    left_hand = models.CharField(max_length=200,blank= True,null=True)
    right_hand = models.CharField(max_length=200,blank= True,null=True)
    marriage = (('Single','Single'),('Married','Married'))
    marital_status = models.CharField(choices=marriage,max_length=10,blank= True,null=True)
    sub = (('yes','yes'),('no','no'))
    spouse_working = models.CharField(choices=sub,max_length=5, blank= True,null=True)
    children = models.IntegerField(blank= True,null=True)
    child_study = models.CharField(choices=sub,max_length=2, blank= True,null=True)
    father = models.CharField(max_length=200,blank= True,null=True)
    mother = models.CharField(max_length=200,blank= True,null=True)
    sibling = models.IntegerField(blank= True,null=True)
    income = models.DecimalField(max_digits=10,decimal_places=2,blank= True,null=True)
    basic_salary = models.DecimalField(max_digits=10,decimal_places=2,blank= True,null=True)
    deduction = models.DecimalField(max_digits=10,decimal_places=2, blank= True,null=True)
    bonus = models.DecimalField(max_digits=10,decimal_places=2, blank= True,null=True)
    total_income = models.DecimalField(max_digits=10,decimal_places=2, blank= True,null=True)
    bank = models.CharField(max_length=100,blank= True,null=True)
    bank_account = models.CharField(max_length=50, blank= True,null=True)
    monthly_pay = models.DecimalField(max_digits=10,decimal_places=2,blank= True,null=True)
    val = (('bank','bank'),('cash','cash'))
    payment_method = models.CharField(choices=val,max_length=200,blank= True,null=True)
    date_of_probation = models.DateField(blank=True,null=True)
    sub_pos=(('Software Developer','Software Developer'),('Software Tester','Software Tester'))
    position = models.CharField(choices=sub_pos,max_length=200,blank= True,null=True)
    sub_dept = (('Hr Department','Hr Department'),('Networking','Networking'),('IT','IT'))
    dept = models.CharField(choices=sub_dept,max_length=100,blank= True,null=True)
    sub_level = (('Junior','Junior'),('Senior','Senior'))
    level = models.CharField(choices=sub_level,max_length=100,blank= True,null=True)
    sub_emp = (('Permanent','Permanent'),('Temporary','Temporary'))
    employee_type = models.CharField(choices=sub_emp,max_length=100,blank= True,null=True)
    holidays = models.CharField(max_length=200,blank= True,null=True)
    workdays = models.CharField(max_length=200,blank= True,null=True)
    date_of_joining = models.DateTimeField(auto_now_add = True,null=True)

    def __str__(self):
        return  self.emp_name.user.username

class leave(models.Model):
    leave = models.ForeignKey(employee,on_delete=models.CASCADE)
    sub = (('CL','CL'),('PL','PL'),('C-off','C-off'),('RH','RH'))
    leave_type = models.CharField(choices=sub,max_length=10,blank=True,null=True)
    CL_balance = models.IntegerField(blank=True,null=True)
    PL_balance = models.IntegerField(blank=True,null=True)
    Coff_balance = models.IntegerField(blank=True,null=True)
    RH_balance = models.IntegerField(blank=True,null=True)
    PL_taken = models.IntegerField(blank=True,null=True)
    CL_taken = models.IntegerField(blank=True,null=True)
    Coff_taken = models.IntegerField(blank=True,null=True)
    RH_taken = models.IntegerField(blank=True,null=True)
    CL_applied = models.IntegerField(blank=True,null=True)
    PL_applied = models.IntegerField(blank=True,null=True)
    Coff_applied = models.IntegerField(blank=True,null=True)
    RH_applied = models.IntegerField(blank=True,null=True)
    CL_rejected = models.IntegerField(blank=True,null=True)
    PL_rejected = models.IntegerField(blank=True,null=True)
    Coff_rejected = models.IntegerField(blank=True,null=True)
    RH_rejected = models.IntegerField(blank=True,null=True)
    valid_until = models.DateField(blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    from_date = models.DateField(blank=True,null=True)
    to_date = models.DateField(blank=True,null=True)
    for_days = models.IntegerField(blank=True,null=True)
    session = models.CharField(max_length=100,blank=True,null=True)
    reason = models.CharField(max_length=100,blank=True,null=True)
    val = (('Approve','Approve'),('Pending','Pending'),('Rejected','Rejected'))
    sub_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    status = models.CharField(choices=val, max_length=10,blank=True,null=True,default="Pending")

    def __str__(self):
        return self.leave.user.username

class leaveAssign(models.Model):
    ltype= models.CharField(max_length=200)
    total = models.IntegerField(null=True)
    date = models.DateField(null=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.ltype
import datetime
class leaveApply(models.Model):
    leave_type = models.ForeignKey(leaveAssign,on_delete=models.CASCADE)
    emp_id = models.ForeignKey(employee,on_delete=models.CASCADE)
    from_date = models.DateField(blank=True,null=True)
    to_date = models.DateField(blank=True,null=True)
    for_days = models.IntegerField(blank=True,null=True)
    val = (('Approve','Approve'),('Pending','Pending'),('Rejected','Rejected'))
    status = models.CharField(choices=val, max_length=10,blank=True,null=True,default="Pending")
    session = models.CharField(max_length=100,blank=True,null=True)
    description = models.CharField(max_length=200,blank=True,null=True)    
    sub_date = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.leave_type.ltype

class document(models.Model):
    doc = models.ForeignKey(employee,on_delete=models.CASCADE)
    sub = (('HR Policy','HR Policy'),('Finance Policy','Finance Policy'))
    workflow = models.CharField(choices=sub, max_length=50,blank=True,null=True)
    document = models.FileField(upload_to='documents/%Y/%m/%d',blank=True,null=True)
    reference = models.CharField(max_length=200,blank=True,null=True)
    description = models.CharField(max_length=300,blank=True,null=True)
    submission_date = models.DateTimeField(auto_now_add=True,null=True)
    sub_status = (('Approve','Approve'),('Pending','Pending'),('Rejected','Rejected'))
    status = models.CharField(choices=sub_status,max_length=10,blank=True,null=True,default="Pending")


class designation(models.Model):
    d_name = models.CharField(max_length=50)
    d_salary = models.DecimalField(max_digits=10,decimal_places=2,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True,null=True)

class Notification(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    status = models.BooleanField(default=False,null=True,blank=True)
    content=models.CharField(max_length=500,null=True,blank=True)
    notify_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.user_id.username  

class HrNotification(models.Model):
    notify_type = models.CharField(max_length=100,null=True,blank=True)
    status = models.BooleanField(default=False,null=True,blank=True)
    content=models.CharField(max_length=500,null=True,blank=True)
    notify_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.content    

class announcement(models.Model):
    hr_announce = models.ForeignKey(HR,on_delete=models.CASCADE)
    status = models.BooleanField(default=False,null=True,blank=True)
    notify_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    text = models.CharField(max_length=200,null=True,blank=True)

    def __str__(self):
        return self.hr_announce.city

class event(models.Model):
    hr_event = models.ForeignKey(HR,on_delete=models.CASCADE)
    status = models.BooleanField(default=False,null=True,blank=True)
    notify_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    event_text = models.CharField(max_length=200,null=True,blank=True)

    def __str__(self):
        return self.hr_event.city

class comments(models.Model):
    text = models.CharField(max_length=200,null=True,blank=True)
    name = models.CharField(max_length=100,null=True,blank=True)
    on_date = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.text


class holiday(models.Model):
    hr_holiday = models.ForeignKey(HR,on_delete=models.CASCADE)
    date = models.DateField(blank=True,null=True)
    sub_day = (('sunday','sunday'),('monday','monday'),('tuesday','tuesday'),('wednesday','wednesday'),('thursday','thursday'),('friday','friday'),('saturday','saturday'))
    day = models.CharField(choices=sub_day,max_length=15,null=True,blank=True)
    holidays = models.CharField(max_length=100,blank=True,null=True)
    sts = (('half','half'),('full','full'))
    status = models.CharField(choices=sts,max_length=10,null=True,blank=True,default=False)
    present_date = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.hr_holiday.city

    
class attendance(models.Model):

    emp_name = models.ForeignKey(employee,on_delete=models.CASCADE)
    check_in_day = models.CharField(max_length=200,blank=True,null=True)
    check_in_month = models.CharField(max_length=200,blank=True,null=True)
    check_in_year = models.CharField(max_length=200,blank=True,null=True)
    check_in_time = models.TimeField(auto_now_add=True,blank=True,null=True)

    check_out_day = models.CharField(max_length=200,blank=True,null=True)
    check_out_month = models.CharField(max_length=200,blank=True,null=True)
    check_out_year = models.CharField(max_length=200,blank=True,null=True)
    check_out_time = models.TimeField(auto_now=True,blank=True,null=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)


    def __str__(self):
        return self.emp_name.user.username

class attendance_user(models.Model):

    emp_name = models.ForeignKey(employee,on_delete=models.CASCADE)
    time=models.TimeField(auto_now_add=True,blank=True,null=True)
    date=models.DateField(auto_now_add=True,blank=True,null=True)
    attendance_type=models.CharField(max_length=200,blank=True,null=True)


    def __str__(self):
        return self.emp_name.user.username

class contactus(models.Model):
    name = models.CharField(max_length=100,null=True,blank=True)
    email = models.CharField(max_length=100,null=True,blank=True)
    subject = models.CharField(max_length=100,null=True,blank=True)
    message = models.CharField(max_length=300,null=True,blank=True)

    def __str__(self):
        return self.name

class myIp(models.Model):
    value = models.CharField(max_length=200)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.value


        







